/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useRef, useMemo, useCallback, useEffect } from 'react';
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    ImageBackground,
} from 'react-native';
import { IcLogo, IcBar, IcBackground, IcClose } from './../assets/index';
import image from './../assets/Image'
import { IPoke, IPokeResponse, MenuPokemon } from './../type/type';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import colors from './../assets/colors/Colors'
import HeaderModal from '../components/HeaderModal';
import CardDetail from '../components/CardDetail';
import CardList from '../components/CardList';
import { connect } from 'react-redux';
import {
    BottomSheetBackdrop,
    BottomSheetModal,
    BottomSheetModalProvider,
} from '@gorhom/bottom-sheet';
import {
    getPokemonList,
    getPokemonType,
    getPokemonListPaging
} from './../redux/screenActions/Home/actions';
import { code_color } from './../utils/ArrayColor';

import i18n from './../i18n';
import i18next from 'i18next';
import Header from '../components/Header';
import axios from 'axios';
import { FlatList } from 'react-native-gesture-handler';






const PesananScreen = props => {
    const [isEnabled, setIsEnabled] = useState<boolean>(false);
    const [listHeader, setHeader] = useState<[{}]>([{
        name: 'Belum dibayar'
    }, {
        name: 'Pesanan aktif'
    }, {
        name: 'Pesanan selesai'
    }, {
        name: 'Pesanan dibatalkan'
    }]);
    const [select, setSelect] = useState('Belum dibayar')
    const isDarkMode = useColorScheme() === 'dark';
    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : colors.white,
    };


    return (
        <SafeAreaView style={[backgroundStyle, { flex: 1 }]} >
            <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
            <View style={{ flexDirection: 'row' }}>
            {listHeader.map((item) => (
                <Pressable onPress={() => setSelect(item.name)} style={{ flex: 1, alignItems: 'center', padding: 10 }}>
                    <Text style={select === item.name ? styles.subTitle : styles.subTitleDisable}>{item.name}</Text>
                    <View  style={select === item.name ? styles.headerActive : null}/>
                </Pressable>
            ))}
            
            </View>
            <View style={{ flex: 1, alignItems: 'center' , justifyContent: 'center'}}>
                <Text style={styles.subTitle}>Kamu belum mempunyai pesanan nih</Text>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    imageCard: { height: 100, width: 100 },
    titleName: { color: '#42494D', fontSize: 14, fontWeight: '700', marginBottom: 10, textAlign: 'left' },
    subTitle: { color: code_color.primary, fontSize: 14, fontWeight: '700', textAlign: 'center' },
    subTitleDisable: { color: 'gray', fontSize: 14, fontWeight: '700', textAlign: 'center' },
    headerActive: { borderWidth: 1, borderColor: code_color.primary, borderStyle: 'solid', width: 100, marginTop: 10}
});
const mapStateToProps = state => {
    const { pokemonList, pokemonData, pokemonType } = state.home;
    return {
        pokemonList, pokemonData, pokemonType
    };
};

export default connect(mapStateToProps, { getPokemonList, getPokemonType, getPokemonListPaging })(PesananScreen);

